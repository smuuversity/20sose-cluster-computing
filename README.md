# 20sose-cluster-computung

This repository holds all work for the module 'Cluster Computing' in spring semester 2020.

## Artifacts

Gather newest artifacts from master at: https://files.smuu.dev/smuuversity/20sose-cluster-computing.

Please consider that this can take up to 10 Minutes.
