#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <string.h>
#include <math.h>

# define T 100 // Time Steps
# define N 10 // # of particles

// define the range of the initialized range
# define P_MAX 10
# define P_MIN 0


// Each Particle has a mas and a position with x and y coordinate
typedef struct{
    //mass
    double mass;
    //position
    double x, y;
    double fx, fy;
    double vx, vy;
}particle;


// barnes hut node struct
typedef struct{
    // the particles of the node
    particle data;

    // pointers to the quadrant of the node
    struct node *nw, *ne, *sw, *se;
}node;



// gravitational force
const double G = 0.000000000066743015;

// one time step?
const double dT = 10;

// count clocks
clock_t begin, end;

void print_particles(particle part[]){
    for(int i = 0; i < N; i++){
        printf("x: %lf y: %lf\n", part[i].x, part[i].y);
    }
    printf("\n");
}


double distance(particle p1, particle p2) {
    return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}


/*
 * https://stackoverflow.com/questions/13408990/how-to-generate-random-double-number-in-c
 */

double double_rand(double min, double max) {
    double scale = rand() / (double) RAND_MAX;
    return min + scale * (max - min);
}


int main() {
    //all particles
    particle *particles = NULL;

    // the root
    node *root;

    // whole tree
    root = (node*) malloc(1 * sizeof(node));


    int num_processors, processor;
    MPI_Datatype particles_type;
    MPI_Datatype node_type;

    MPI_Init(NULL, NULL);

    MPI_Comm_rank(MPI_COMM_WORLD, &processor);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);


    int send_cnts[num_processors];
    int displ_cnts[num_processors];
    int remainder = N % (num_processors);


    int split = 1;
    //calculate sendcounts and displacements
    //Every process gets one more particles as long as the remainder is zero
    for(int i = 0; i < num_processors; i++){
        if(remainder == 0){
            split = 0;
        }
        send_cnts[i] = (N / num_processors) + split;
        remainder--;
    }


    displ_cnts[0] = 0;
    for(int i = 1; i < num_processors; i++){
        displ_cnts[i] = displ_cnts[i-1] + send_cnts[i-1];
    }

    //init particles by master
    if(processor == 0){
        particles = (particle*) malloc(N * sizeof(particle));
        srand(time(NULL));
        for (int i = 0; i < N; i++) {
            particles[i].mass = 1.0; //mass of 1, velocity and force of 0 and a random position
            particles[i].vx = 0;
            particles[i].vy = 0;
            particles[i].fx = 0;
            particles[i].fy = 0;
            particles[i].x = double_rand(P_MIN, P_MAX);
            particles[i].y = double_rand(P_MIN, P_MAX);

            root->data = particles[i];
        }
        print_particles(particles);

        //TODO: Construct Barnes Hut Tree on master



        // begin counting the clocks after init
        begin = clock();
    }

    //MPI Dataype to send particle struct
    MPI_Type_contiguous(7, MPI_DOUBLE, &particles_type);
    MPI_Type_commit(&particles_type);

    MPI_Type_contiguous(1, particles_type, &node_type);
    MPI_Type_commit(&node_type);


    //Broadcast tree to all nodes
    MPI_Bcast(root, 1, node_type, 0, MPI_COMM_WORLD);


    //TODO: Each Process choose disjoint subset of particles and calculate new forces and positions

    //TODO: Gather all particles to master


    if(processor == 0){
        end = clock();
        printf("Clocks: %d\n\n", end - begin);

        print_particles(particles);
    }


    MPI_Finalize();
    return 0;
}

