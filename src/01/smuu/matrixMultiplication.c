#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[]) {

  if (argc != 4) {
    printf("./matrixMultiplication <m> <q> <n>\n");
    return 1;
  }

  const int m = atoi(argv[1]);
  const int q = atoi(argv[2]);
  const int n = atoi(argv[3]);

  // initiate random generator with actual time
  srand(time(NULL));

  // init array A
  double a_arr_2d[m][q];
  // fill A with random numbers
  for(int i = 0; i < m; i++) {
    for(int j = 0; j < q; j++) {
      a_arr_2d[i][j] = (double) rand() / RAND_MAX * 50.0;
    }
  }

  // init array B
  double b_arr_2d[q][n];
  // fill B with random numbers
  for(int i = 0; i < q; i++) {
    for(int j = 0; j < n; j++) {
      b_arr_2d[i][j] = (double) rand() / RAND_MAX * 50.0;
    }
  }

  double row, column;

  row = sizeof(a_arr_2d) / sizeof(a_arr_2d[0]);
  column = sizeof(a_arr_2d[0])/sizeof(a_arr_2d[0][0]);
  printf("Array A(%f, %f):\n", row, column);
  for(int i = 0; i < row; i++) {
    for(int j = 0; j < column; j++) {
      printf("%f ", a_arr_2d[i][j]);
      if(j == column-1) {
        printf("\n");
      }
    }
  }
  row = sizeof(b_arr_2d) / sizeof(b_arr_2d[0]);
  column = sizeof(b_arr_2d[0])/sizeof(b_arr_2d[0][0]);
  printf("Array B(%f, %f):\n", row, column);
  for(int i = 0; i < row; i++) {
    for(int j = 0; j < column; j++) {
      printf("%f ", b_arr_2d[i][j]);
      if(j == column-1) {
        printf("\n");
      }
    }
  }

  // MULTIPLICATION
  // init array C
  double c_arr_2d[m][n];
  // for all entries in target matrix
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {

      double entry = 0;
      for (int k = 0; k < q; k++) {
        entry += a_arr_2d[i][k] * b_arr_2d[k][j];
      }
      c_arr_2d[i][j] = entry;
    }
  }

  row = sizeof(c_arr_2d) / sizeof(c_arr_2d[0]);
  column = sizeof(c_arr_2d[0])/sizeof(c_arr_2d[0][0]);
  printf("Array C(%f, %f) = A + B:\n", row, column);
  for(int i = 0; i < row; i++) {
    for(int j = 0; j < column; j++) {
      printf("%f ", c_arr_2d[i][j]);
      if(j == column-1) {
        printf("\n");
      }
    }
  }

  return 0;
}
