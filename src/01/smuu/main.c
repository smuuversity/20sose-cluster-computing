#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

#define m 1000
#define p 2000
#define n 1000

int rank, size;
int mat_a[m][p], mat_b[p][n], mat_c[m][n];
clock_t begin, end;

int random_number() {
    srand(time(NULL));
    return rand() % 99;
}


int init_matrix() {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < p; j++) {
            mat_a[i][j] = 1;
        }
    }
    for (int i = 0; i < p; i++) {
        for (int j = 0; j < n; j++) {
            mat_b[i][j] = 2;
        }
    }
    return 0;
}

void print_matrix(int row, int col, int mat[row][col]) {
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            printf(" %d ", mat[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


int main() {

    init_matrix();


    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int rows, remainder;
    int offset = 0;

    // calculate the workload of each worker
    rows = m / (size - 1);
    remainder = m % (size - 1);


// ############### master ###############

    if (rank == 0) {
        begin = clock();
        //send rows to workers
        for (int i = 1; i < size; i++) {

            // If there is a remainder, the last worker has more to do.
            // This could be equally distributed
            if (i == (size - 1)) {
                rows += remainder;
            }

            // send offset, no. of rows, and actual rows
            // and the whole Matrix b
            MPI_Send(&offset, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(&rows, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(&mat_a[offset][0], p * rows, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(&mat_b, p * n, MPI_INT, i, 0, MPI_COMM_WORLD);

            offset += rows;

        }

        // Broadcast Matrix b to all processes
       // MPI_Bcast(mat_b, p * n, MPI_INT, 0, MPI_COMM_WORLD);


        //receive all results from workers
        for (int i = 1; i < size; i++) {
            MPI_Recv(&offset, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
            MPI_Recv(&rows, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
            MPI_Recv(&mat_c[offset][0], n * rows, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
        }


        //printf("Received Results\n");
        //print_matrix(m, p, mat_a);
        //print_matrix(p, n, mat_b);
        //print_matrix(m, n, mat_c);
        end = clock();
        printf("Clocks: %d\n", end - begin);
    } // end of master




// ############### worker ###############
    if (rank > 0) {

        // The last worker has more to do
        if (rank == (size - 1)) {
            rows += remainder;
        }

        //receive rows from master
        MPI_Recv(&offset, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
        MPI_Recv(&rows, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
        MPI_Recv(&mat_a[offset][0], p * rows, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
        MPI_Recv(&mat_b, p * n, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);


        // multiply
        int sum = 0;
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < rows; k++) {
                for (int j = 0; j < p; j++) {

                    sum += mat_a[k][j] * mat_b[i][j];

                }
                mat_c[k][i] = sum;
                sum = 0;
            }
        }

        // send the results back to the master
        MPI_Send(&offset, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&rows, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&mat_c, n * rows, MPI_INT, 0, 0, MPI_COMM_WORLD);

    }// end of worker


    MPI_Finalize();


    return 0;
}

