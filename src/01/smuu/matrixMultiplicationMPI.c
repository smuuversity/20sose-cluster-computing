#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <mpi.h>

int getWorkloadPer(int *workloadPer, int workload, int size, int processor);

int main(int argc, char *argv[]) {

  int i, j, k;

  int my_rank, size, tag = 0;
  MPI_Status status;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (argc != 4) {
    printf("./matrixMultiplicationMPI <m> <q> <n>\n");
    return 1;
  }

  const int m = atoi(argv[1]);
  const int q = atoi(argv[2]);
  const int n = atoi(argv[3]);

  if (my_rank == 0) {

    printf("Run with %d processors\n", size);

    // initiate random generator with actual time
    srand(time(NULL));

    // init array A
    double a_arr_2d[m][q];
    // fill A with random numbers
    for(i = 0; i < m; i++) {
      for(j = 0; j < q; j++) {
        a_arr_2d[i][j] = (double) rand() / RAND_MAX * 50.0;
      }
    }

    // init array B
    double b_arr_2d[q][n];
    // fill B with random numbers
    for(i = 0; i < q; i++) {
      for(j = 0; j < n; j++) {
        b_arr_2d[i][j] = (double) rand() / RAND_MAX * 50.0;
      }
    }

    // int row, column;

    // print A
    // row = sizeof(a_arr_2d) / sizeof(a_arr_2d[0]);
    // column = sizeof(a_arr_2d[0])/sizeof(a_arr_2d[0][0]);
    // printf("Array A(%d, %d):\n", row, column);
    // for(i = 0; i < row; i++) {
    //   for(j = 0; j < column; j++) {
    //     printf("%f ", a_arr_2d[i][j]);
    //     if(j == column-1) {
    //       printf("\n");
    //     }
    //   }
    // }
    // print B
    // row = sizeof(b_arr_2d) / sizeof(b_arr_2d[0]);
    // column = sizeof(b_arr_2d[0])/sizeof(b_arr_2d[0][0]);
    // printf("Array B(%d, %d):\n", row, column);
    // for(i = 0; i < row; i++) {
    //   for(j = 0; j < column; j++) {
    //     printf("%f ", b_arr_2d[i][j]);
    //     if(j == column-1) {
    //       printf("\n");
    //     }
    //   }
    // }

    time_t start = time(0);
    printf("Start to calculate at %ld\n", start);

    // amount of rows that are already allocated
    int rows_allocated = 0;

    // calculate how much rows need to calculated on root
    int calc_0;
    getWorkloadPer(&calc_0, m, size, my_rank);

    rows_allocated += calc_0;

    for(i = 0; i < size; i++) {
      int calc_i;
      getWorkloadPer(&calc_i, m, size, i);

      printf("%d: Processor #%d will calculate %d rows\n", my_rank, i, calc_i);
    }

    // loop through all rest of processors
    for(i = 1; i < size; i++) {
      // calculate how much rows need to calculated on this processor i
      int calc_i;
      getWorkloadPer(&calc_i, m, size, i);

      // broadcast matrix A

      double a_part_arr_2d[calc_i][q];
      for(j = 0; j < calc_i; j++) {
        memcpy(&a_part_arr_2d[j], &a_arr_2d[rows_allocated + j], sizeof(a_arr_2d[j]));
      }

      MPI_Send(a_part_arr_2d, calc_i * q, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);

      MPI_Send(b_arr_2d, q * n, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);

      rows_allocated += calc_i;
    }

    // init array C
    double c_arr_2d[m][n];
    int rows_calculated = 0;

    // calculate own part
    printf("I'm %d and I will calculate %d rows\n", my_rank, calc_0);
    for(i = 0; i < calc_0; i++) {
      for(j = 0; j < n; j++) {

        double entry = 0;
        for(k = 0; k < q; k++) {
          entry += a_arr_2d[i][k] * b_arr_2d[k][j];
        }
        c_arr_2d[i][j] = entry;
      }
    }

    rows_calculated += calc_0;

    // receive parts
    // loop through all rest of processors
    for (i = 1; i < size; i++) {
      // calculate how much this processor calculated
      int calc_i;
      getWorkloadPer(&calc_i, m, size, i);

      double c_part_arr_2d[calc_i][n];

      MPI_Recv(c_part_arr_2d, calc_i * n, MPI_DOUBLE, i, tag, MPI_COMM_WORLD, &status);

      for (j = 0; j < calc_i; j++) {
        memcpy(&c_arr_2d[rows_calculated + j], &c_part_arr_2d[j], sizeof(c_arr_2d[j]));
      }

      rows_calculated += calc_i;
    }

    time_t end = time(0);
    printf("End to calculate at %ld; Took %ld Seconds\n", end, end - start);

    // // print result
    // int row = sizeof(c_arr_2d) / sizeof(c_arr_2d[0]);
    // int column = sizeof(c_arr_2d[0])/sizeof(c_arr_2d[0][0]);
    // printf("Array C(%d, %d) = A + B:\n", row, column);
    // for(i = 0; i < row; i++) {
    //   for(j = 0; j < column; j++) {
    //     printf("%f ", c_arr_2d[i][j]);
    //     if(j == column-1) {
    //       printf("\n");
    //     }
    //   }
    // }

    // // TEST RESULT
    // // for all entries in target matrix
    // for (i = 0; i < m; i++) {
    //   for (j = 0; j < n; j++) {

    //     double entry = 0;
    //     for ( k = 0; k < q; k++) {
    //       entry += a_arr_2d[i][k] * b_arr_2d[k][j];
    //     }
    //     c_arr_2d[i][j] = entry;
    //   }
    // }

    // row = sizeof(c_arr_2d) / sizeof(c_arr_2d[0]);
    // column = sizeof(c_arr_2d[0])/sizeof(c_arr_2d[0][0]);
    // printf("Array C(%d, %d) = A + B:\n", row, column);
    // for(i = 0; i < row; i++) {
    //   for(j = 0; j < column; j++) {
    //     printf("%f ", c_arr_2d[i][j]);
    //     if(j == column-1) {
    //       printf("\n");
    //     }
    //   }
    // }

  } else if (my_rank > 0) {
    // calculate how much need to calculated on this processor
    int calc_i;
    getWorkloadPer(&calc_i, m, size, my_rank);

    double a_part_arr_2d[calc_i][q];
    MPI_Recv(a_part_arr_2d, calc_i * q, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);

    double b_arr_2d[q][n];
    MPI_Recv(b_arr_2d, q * n, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);

    printf("I'm %d and I will calculate %d rows\n", my_rank, calc_i);

    double c_part_arr_2d[calc_i][n];
    // calculate part
    for(i = 0; i < calc_i; i++) {
      for(j = 0; j < n; j++) {

        double entry = 0;
        for(k = 0; k < q; k++) {
          entry += a_part_arr_2d[i][k] * b_arr_2d[k][j];
        }
        c_part_arr_2d[i][j] = entry;
      }
    }

    // send result
    MPI_Send(c_part_arr_2d, calc_i * n, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
  }


  MPI_Finalize();

  return 0;
}

int getWorkloadPer(int *workloadPer, int workload, int size, int processor) {
  int calc_i = workload / size;
    if (workload % size > processor)
      calc_i++;

  *workloadPer = calc_i;
  return 0;
}
