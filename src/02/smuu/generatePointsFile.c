#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "point.h"
#include "pointList.h"

int randomPoints();
double double_rand(double min, double max);
int randomStruct(struct Point *p);

int n; // amount of points
const int x = 10000; // size of area
const int y = 10000; // size of area

const int minW = 10000; // min weight
const int maxW = 10000000; // max weight

struct Point *points; // array of all points (if mass=0 then point not exist)

int main(int argc, char *argv[]) {

  srand(time(NULL));

  if (argc != 3) {
    printf("./generatePointsFile <fileName> <amount>\n");
    return 1;
  }
  const char* name = argv[1];
  const int amount = atoi(argv[2]);
  n = amount;
  printf("Write to %s\n", name);

  points = malloc(amount * sizeof(struct Point));
  randomPoints();

  FILE *fp;
  fp = fopen(name, "w+");
  int k;
  struct Point *p;
  for (k = 0; k < amount; ++k) {
    p = &points[k];
    fprintf(fp, "%f,%f,%f,%f,%f\n", p->mass, p->pos[0], p->pos[1], p->velocity[0], p->velocity[1]);
  }
  fclose(fp);

  return 0;
}

int randomPoints() {
  int i;
  for (i = 0; i < n; ++i) {
    randomStruct(&points[i]);
  }
  return 1;
}

/*
 * https://stackoverflow.com/questions/13408990/how-to-generate-random-double-number-in-c
 */
double double_rand(double min, double max) {
  double scale = rand() / (double) RAND_MAX;
  return min + scale * (max - min);
}

int randomStruct(struct Point *p) {
  p->mass = double_rand(minW, maxW);
  p->pos[0] = double_rand(0, x);
  p->pos[1] = double_rand(0, y);
  p->velocity[0] = double_rand(0, 0);
  p->velocity[1] = double_rand(0, 0);
  return 0;
}
