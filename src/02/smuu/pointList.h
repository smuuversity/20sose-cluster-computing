struct PointList {
    struct Point point;
    struct PointList *next;
    struct PointList *prev;
};