#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <mpi.h>

#include "nbody.h"
#include "point.h"
#include "pointList.h"

int readInData(char* fileName);
int init();
int step();
int exist(struct Point *p);
int printPoints(struct Point *pointsLocal);
int printPoint(struct Point *p);
double distance(struct Point *p1, struct Point *p2);
int checkCollison(struct Point *p1, struct Point *p2);
int isPoint(struct Point *p);
int countPoints();
int pointToArray(struct Point *p, double *array);
int arrayToPoint(double *array, struct Point *p);
int getWorkloadPer(int *workloadPer, int workload, int size, int processor);

static int debug = 0;

const double G = 0.000000000066743015; // Gravitational constant

const double dT = 10; // time steps in seconds

int n; // amount of points
const int x = 10000; // size of area
const int y = 10000; // size of area

const int minW = 10000; // min weight
const int maxW = 10000000; // max weight

struct Point *points; // array of all points (if mass=0 then point not exist)

int main(int argc, char *argv[]) {

  int rank, size, tag = 0;
  MPI_Status status;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (argc != 2) {
    printf("./generatePointsFile <fileName>\n");
    return 1;
  }

  if (rank == 0) {
    struct Point p;
    p.mass = 10.0;
    p.pos[0] = 50.0;
    p.pos[1] = 75.0;
    p.velocity[0] = 0.1;
    p.velocity[1] = 0.2;
    double point[5];
    pointToArray(&p, point);
    printf("%ld, %ld\n", sizeof(point), sizeof(sizeof(double)) * 5);
    MPI_Send(point, 5, MPI_DOUBLE, 1, tag, MPI_COMM_WORLD);
  } else if (rank == 1) {
    struct Point p;
    double point[5];
    MPI_Recv(point, 5, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
    arrayToPoint(point, &p);
    printPoint(&p);
  }

  // readInData(argv[1]);


  // long time, start, end;

  // start = clock();
  // printf("simulating");
  // fflush(stdout); // flush stdout as no new line
  // int dot = 0;
  // while (countPoints() > 1) {
  //   step();
  //   dot++;
  //   if (dot > 10000) {
  //     printf(".");
  //     fflush(stdout); // flush stdout as no new line
  //     dot = 0;
  //   }
  // }
  // printf("\n");
  // end = clock();
  // time = (end - start);

  // printf("Time to calculate: %ld ticks\n", time);

  MPI_Finalize();

  return 0;
}

int readInData(char *fileName) {
  printf("Read data from %s\n", fileName);
  struct Point *p;
  FILE *fp;
  char * line = NULL;
  size_t len = 0;
  size_t read;
  int amount = 0;
  int count = 0;
  fp = fopen(fileName, "r");
  // count lines
  while ((read = getline(&line, &len, fp)) != -1) {
    amount++;
  }
  fclose(fp);
  n = amount;

  points = malloc(amount * sizeof(struct Point));

  fp = fopen(fileName, "r");
  // read points
  while ((read = getline(&line, &len, fp)) != -1) {
    p = &points[count];
    p->mass = strtod(strtok(line, ","), NULL);
    p->pos[0] = strtod(strtok(NULL, ","), NULL);
    p->pos[1] = strtod(strtok(NULL, ","), NULL);
    p->velocity[0] = strtod(strtok(NULL, ","), NULL);
    p->velocity[1] = strtod(strtok(NULL, ",\n"), NULL);
    count++;
  }
  fclose(fp);
  printf("Read %d lines\n", amount);
  return 0;
}

int step() {
  int i, j;
  double X, Y;
  double a;
  struct Point *p, *p2;

  // TODO: fix double calculation
  // calc Force of all points
  //#pragma omp for schedule (static)
  for (i = 0; i < n; ++i) {
    p = &points[i];

    X = 0;
    Y = 0;
    for (j = 0; j < n; ++j) {
      if (exist(&points[i]) && exist(&points[j])) {
        if (i != j) { // dont add for itself
          p2 = &points[j];
          a = ((p->mass * p2->mass) / pow(distance(p, p2), 2));
          X += a * (p->pos[0] - p2->pos[0]);
          Y += a * (p->pos[1] - p2->pos[1]);
        }
      }
    }
    // tmp save force to points
    p->force[0] = -G * X;
    p->force[1] = -G * Y;
  }


  int k, l;
  double newX, newY;
  for (k = 0; k < n; ++k) {

    p = &points[k];

    // calc new velocity
    p->velocity[0] = p->velocity[0] + (dT / p->mass * p->force[0]);
    p->velocity[1] = p->velocity[1] + (dT / p->mass * p->force[1]);

    // calc new pos
    newX = p->pos[0] + (dT * p->velocity[0]);
    newY = p->pos[1] + (dT * p->velocity[1]);

    p->pos[0] = newX;
    p->pos[1] = newY;

    // if collide with corner -> throw back
    if (newX <= 0 || newX >= x) {
      // left or right
      p->velocity[0] = -p->velocity[0];
      p->velocity[1] = p->velocity[1];
    } else if (newY <= 0 || newY >= y) {
      // top o bottom
      p->velocity[0] = p->velocity[0];
      p->velocity[1] = -p->velocity[1];
    }

    // check collision between all points
    for (l = 0; l < n; ++l) {
      p2 = &points[l];
      if (exist(p) && exist(p2)) {
        if (k != l) { // dont check for itself
          if (checkCollison(p, p2)) {
            if (debug) {
              printf("Collision between\n");
              printPoint(p);
              printPoint(p2);
            }

            p->velocity[0] =
                    (p->mass * p->velocity[0] + p2->mass * p2->velocity[0]) / (p->mass + p2->mass);
            p->velocity[1] =
                    (p->mass * p->velocity[1] + p2->mass * p2->velocity[1]) / (p->mass + p2->mass);
            p->mass += p2->mass;

            // deleting other point
            p2->mass = 0; // mass = 0 means not exist

            if (debug) {
              printf("New point:\n");
              printPoint(p);
              printf("%d points left\n", countPoints());
            }
          }
        }
      }
    }
  }
  return 0;
}

int exist(struct Point *p) {
    // a point exist if mass > 0
    return p->mass > 0;
}

int printPoints(struct Point *pointsLocal) {
  int i;
  printf("\n\n");
  printf("All points:\n");
  for (i = 0; i < n; ++i) {
    if (isPoint(&pointsLocal[i])) {
      printPoint(&pointsLocal[i]);
    }
  }
  printf("\n\n");
  return 0;
}

int printPoint(struct Point *p) {
  printf("Point: mass: %g, pos: (%g, %g), velocity: (%g, %g)\n", p->mass, p->pos[0], p->pos[1], p->velocity[0], p->velocity[1]);
  return 0;
}

/*
 * euclid
 */
double distance(struct Point *p1, struct Point *p2) {
    return sqrt(pow(p1->pos[0] - p2->pos[0], 2) + pow(p1->pos[1] - p2->pos[1], 2));
}

int checkCollison(struct Point *p1, struct Point *p2) {
    if (isPoint(p1) && isPoint(p2)) {
        if (distance(p1, p2) <= 1) {
            return 1;
        }
    }
    return 0;
}

int isPoint(struct Point *p) {
    // a point is not a point if mass = 0
    return p->mass > 0;
}

int countPoints() {
    // FIXME: dont calc, save as variable
    int a = 0, i;

    for (i = 0; i < n; ++i) {
        if (isPoint(&points[i])) {
            a++;
        }
    }
    return a;
}

int pointToArray(struct Point *p, double *array) {
  array[0] = p->mass;
  array[1] = p->pos[0];
  array[2] = p->pos[1];
  array[3] = p->velocity[0];
  array[4] = p->velocity[1];
  return 0;
}

int arrayToPoint(double *array, struct Point *p) {
  p->mass = array[0];
  p->pos[0] = array[1];
  p->pos[1] = array[2];
  p->velocity[0] = array[3];
  p->velocity[1] = array[4];
  return 0;
}

/**
 * int workload: size of the problem
 * int size: amount of processors
 * int processor: identifier of the processor to want the workload for
 */
int getWorkloadPer(int *workloadPer, int workload, int size, int processor) {
  int calc_i = workload / size;
  if (workload % size > processor)
    calc_i++;

  *workloadPer = calc_i;
  return 0;
}
