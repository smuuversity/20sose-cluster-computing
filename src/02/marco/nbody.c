#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <string.h>
#include <math.h>

# define T 1000 // Time Steps
# define N 10 // # of particles
# define P_MAX 10
# define P_MIN 0


// Each Particle has a mas and a position with x and y coordinate
typedef struct{
    //mass
   double mass;
   //position
   double x, y;
   double fx, fy;
   double vx, vy;
}particle;

// gravitational force
const double G = 0.000000000066743015;

// one time step?
const double dT = 10;

// count clocks
clock_t begin, end;

void print_particles(particle part[]){
    for(int i = 0; i < N; i++){
        printf("x: %lf y: %lf\n", part[i].x, part[i].y);
    }
    printf("\n");
}


double distance(particle p1, particle p2) {
    return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}


/*
 * https://stackoverflow.com/questions/13408990/how-to-generate-random-double-number-in-c
 */

double double_rand(double min, double max) {
    double scale = rand() / (double) RAND_MAX;
    return min + scale * (max - min);
}


int main() {
    //all particles
    particle *particles = NULL;



    int num_processors, processor;
    MPI_Datatype my_MPI_type;

    MPI_Init(NULL, NULL);

    MPI_Comm_rank(MPI_COMM_WORLD, &processor);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);


    int send_cnts[num_processors];
    int displ_cnts[num_processors];
    int remainder = N % (num_processors);


    int split = 1;
    //calculate sendcounts and displacements
    //Every process gets one more particles as long as the remainder is zero
    for(int i = 0; i < num_processors; i++){
        if(remainder == 0){
            split = 0;
        }
        send_cnts[i] = (N / num_processors) + split;
        remainder--;
        // printf("%d - %d : Index %d\n",processor, send_cnts[i], i);
    }


    displ_cnts[0] = 0;
    for(int i = 1; i < num_processors; i++){
        displ_cnts[i] = displ_cnts[i-1] + send_cnts[i-1];
        //printf("%d\n", displ_cnts[i]);
    }

    //init particles by master
    if(processor == 0){
        particles = (particle*) malloc(N * sizeof(particle));
        srand(time(NULL));
        for (int i = 0; i < N; i++) {
            particles[i].mass = 1.0; //mass of 1, velocity and force of 0 and a random position
            particles[i].vx = 0;
            particles[i].vy = 0;
            particles[i].fx = 0;
            particles[i].fy = 0;
            particles[i].x = double_rand(P_MIN, P_MAX);
            particles[i].y = double_rand(P_MIN, P_MAX);


        }
        print_particles(particles);

        // begin counting the clocks after init
        begin = clock();
    }





    //MPI Dataype to send particle struct
    MPI_Type_contiguous(7, MPI_DOUBLE, &my_MPI_type);
    MPI_Type_commit(&my_MPI_type);

    // each processor only needs an array that is as big as the associated send_cnt
    particle *local_particles = NULL;
    local_particles = malloc(send_cnts[processor] * sizeof(particle));

    // now distribute the particles to all processors
    MPI_Scatterv(particles, send_cnts, displ_cnts, my_MPI_type, local_particles,
                 send_cnts[processor], my_MPI_type, 0, MPI_COMM_WORLD);

    // particles is obsolete now
    if(processor == 0) {
        free(particles);
    }


    // Now every processor has some particles and calculates the new positions for them
    // Iterate over time T
    for(int i = 0; i < T; i++){

        // acceleration a
        double a;
        double X, Y;

        // pipelining the force computation
        for(int pipe_step = 0; pipe_step < num_processors; pipe_step++){

            //first, compute forces of the local_particles
            if(pipe_step == 0){
                for(int p1 = 0; p1 < send_cnts[processor]; p1++){
                    for(int p2 = 0; p2 < send_cnts[processor]; p2++){
                        if(p1 != p2){
                            a = ((local_particles[p1].mass * local_particles[p2].mass)
                                    / pow(distance(local_particles[p1], local_particles[p2]), 2));
                            X += a * (local_particles[p1].x - local_particles[p2].x);
                            Y += a * (local_particles[p1].y - local_particles[p2].y);

                        }
                    }
                    local_particles[p1].fx = X;
                    local_particles[p1].fy = Y;
                }
            }

            if(pipe_step != 0) {
                // if pipe_step is not the beginning, each processor sends its particles to the "next"
                // and receives the particles from the "previous"
                int destination = (processor - pipe_step + num_processors) % (num_processors);
                int arrival = (processor + pipe_step + num_processors) % num_processors;
                particle *buf_particles;
                buf_particles = malloc(send_cnts[arrival] * sizeof(particle));

                MPI_Send(local_particles, send_cnts[processor], my_MPI_type, destination, 0, MPI_COMM_WORLD);
                MPI_Recv(buf_particles, send_cnts[arrival], my_MPI_type, arrival, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);

                for(int p1 = 0; p1 < send_cnts[processor]; p1++) {
                    for (int p2 = 0; p2 < send_cnts[arrival]; p2++) {
                        a = ((local_particles[p1].mass * buf_particles[p2].mass)
                                / pow(distance(local_particles[p1], buf_particles[p2]), 2));
                        X += a * (local_particles[p1].x - buf_particles[p2].x);
                        Y += a * (local_particles[p1].y - buf_particles[p2].y);

                    }
                    local_particles[p1].fx += X;
                    local_particles[p1].fy += Y;
                }
            }
        }

        // after the stages calculate the new positions of each local particle
        for(int k = 0; k < send_cnts[processor]; k++){
            local_particles[k].fx *= -G;
            local_particles[k].fy *= -G;

            local_particles[k].vx = local_particles[k].vx + (dT / local_particles[k].mass * local_particles[k].fx);
            local_particles[k].vy = local_particles[k].vy + (dT / local_particles[k].mass * local_particles[k].fy);

            local_particles[k].x = local_particles[k].x + (dT * local_particles[k].vx);
            local_particles[k].y = local_particles[k].y + (dT * local_particles[k].vy);
        }
    }


    // At the end Gather all particles back to the master
    if(processor == 0){
        // allocate the particles array
        particles = (particle*) malloc(N * sizeof(particle));

    }

    MPI_Gatherv(local_particles, send_cnts[processor], my_MPI_type, particles, send_cnts, displ_cnts, my_MPI_type, 0, MPI_COMM_WORLD);


    if(processor == 0){
        end = clock();
        printf("Clocks: %ld\n\n", end - begin);
        for(int i = 0; i < N; i++){
            printf("x: %lf \t y: %lf\n", particles[i].x, particles[i].y);
        }
    }

    MPI_Finalize();
    return 0;
}

